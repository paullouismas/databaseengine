export class DatabaseIdentity {
	private static _tables: { [key: string]: number } = {};

	public static next(tableName: string): number {
		if (!(tableName in this._tables)) {
			return this._tables[tableName] = 0;
		}

		return this._tables[tableName] += 1;
	}
}