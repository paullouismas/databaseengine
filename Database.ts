import { DatabaseTable } from "./DatabaseTable.js";
import { TableDefinition } from "./TableDefinition.js";
import { TableCreationError, TableInsertError, TableError } from "./Errors.js";
import { DatabaseValueEntry } from "./DatabaseValueEntry.js";
import { DatabaseConfig } from "./DatabaseConfig.js";

export class Database {
	private _tables: { [key: string]: DatabaseTable } = {};
	
	private _name: string;

	public constructor(
		name: string
	) {
		this._name = name;
	}

	public createTable(
		tableName: string, 
		tableDefinition: TableDefinition, 
		identityCount: number = 0
	): void {
		if (this.exist(tableName)) {
			throw new TableCreationError(`Unable to create table '${tableName}', table already exist.`);
		}

		this._tables[tableName] = new DatabaseTable(tableName, tableDefinition, identityCount);
	}

	public insert(
		tableName: string, 
		values: DatabaseValueEntry, 
		config: DatabaseConfig = {}
	): void {
		if (!this.exist(tableName)) {
			throw new TableInsertError(`Unable to insert into table '${tableName}', table does not exist.`);
		}

		this._tables[tableName].insert(values, config);
	}

	public select(
		tableName: string, 
		columns: { [key: string]: { [key: string]: string } } | "*", 
		join: object, 
		conditions: string[]
	): DatabaseValueEntry[] {
		if (!this.exist(tableName)) {
			throw new TableInsertError(`Unable to select from table '${tableName}', table does not exist.`);
		}

		// #TODO
		
		return this._tables[tableName].select(columns[tableName] || columns, join, conditions);
	}

	public delete(
		tableName: string, 
		conditions: string[]
	): void {
		if (!this.exist(tableName)) {
			throw new TableInsertError(`Unable to delete from table '${tableName}', table does not exist.`);
		}

		this._tables[tableName].delete(conditions);
	}

	public update(
		tableName: string, 
		values: DatabaseValueEntry, 
		conditions: string[]
	): void {
		if (!this.exist(tableName)) {
			throw new TableInsertError(`Unable to update from table '${tableName}', table does not exist.`);
		}

		this._tables[tableName].update(values, conditions);
	}

	public dropTable(
		tableName: string
	): void {
		if (!this.exist(tableName)) {
			throw new TableError(`Unable to drop table '${tableName}', table does not exist.`);
		}

		delete this._tables[tableName];
	}

	public exist(
		tableName: string
	): boolean {
		return tableName in this._tables;
	}
}