import { DatabaseCreationError, DatabaseUseError, DatabaseError, TableCreationError } from "./Errors.js";
import { Database } from "./Database.js";
import { TableDefinition } from "./TableDefinition.js";
import { DatabaseValueEntry } from "./DatabaseValueEntry.js";
import { DatabaseSyncFunction } from "./DatabaseSyncFunction.js";
import { assertString, assert, parseProp, assertDefinition } from "./Utils.js";

/**
 * Requires the following modules
 * @class The main structure
 * 
 * For errors:
 * @requires DatabaseCreationError
 * @requires DatabaseUseError
 * @requires DatabaseError
 * @requires TableCreationError
 * 
 * For structure:
 * @requires Database
 * @requires TableDefinition
 * @requires DatabaseValueEntry
 * @requires DatabaseSyncFunction
 * 
 * For assertions:
 * @requires assertString
 * @requires assert
 * @requires parseProp
 */
export class DB {
	/**
	 * The object representing the databases
	 * @private {object}
	 */
	private _DBs: { [key: string]: Database } = {
		master: new Database("master")
	};

	/**
	 * The name of the current database
	 * @private {string}
	 */
	private _currentDB: string = "master";

	/**
	 * The lists of functions to call on synchronize
	 * @private {DatabaseSyncFunction[]}
	 */
	private _syncFunc: DatabaseSyncFunction[] = [];

	/**
	 * The constructor to generate an instance of DB
	 * @constructor
	 */
	public constructor() {}

	/**
	 * Create either a database or a table
	 * @param {string} target Whether to create a database or a table
	 * @param {string} name The name of the object to create
	 * @param {object} definition Optionnal. The definition of the table
	 */
	public create(
		target: "database" | "table", 
		name: string, 
		definition: TableDefinition | undefined
	): void {
		if (target === "database") {
			assertString(name, `Invalid type of database name '${typeof name}', it must be a string.`);

			if (/[^\w]/.test(name)) {
				throw new DatabaseCreationError(`Invalid character(s) in database name '${name}', it must only contain alphanumeric characters.`);
			}

			if (this.exist("database", name)) {
				throw new DatabaseCreationError(`Unable to create database '${name}', the database already exist.`);
			}

			this._DBs[name] = new Database(name);
		} else if (target === "table") {
			assertString(name, `Invalid type of table name '${typeof name}', it must be a string.`);
			assertDefinition(definition as TableDefinition);

			if (!this.exist("database", this._currentDB)) {
				throw new DatabaseUseError(`No database is currently in use.`);
			}

			if (/[^\w]/.test(name)) {
				throw new TableCreationError(`Invalid character(s) in table name '${name}', it must only contain alphanumeric characters.`);
			}

			this._DBs[this._currentDB].createTable(name, definition as TableDefinition);
		} else {
			throw new DatabaseError(`Invalid target '${target}', target must be either 'database' or 'table'.`);
		}
	}

	/**
	 * Drop either a database or a table
	 * @param {string} target Whether to drop a database or a table
	 * @param {string} name The name of the object to drop
	 */
	public drop(
		target: "database" | "table", 
		name: string
	): void {
		if (target === "database") {
			assertString(name, `Invalid type of database name '${typeof name}', it must be a string.`);
			
			if (!this.exist("database", name)) {
				throw new DatabaseError(`Unable to drop database '${name}', the database does not exist.`);
			}

			if (name === this._currentDB) {
				throw new DatabaseError(`Unable to drop database '${name}', the database is currently in use.`);
			}

			delete this._DBs[name];
		} else if (target === "table") {
			assertString(name, `Invalid type of table name '${typeof name}', it must be a string.`);

			if (!this.exist("database", name)) {
				throw new DatabaseUseError(`No database is currently in use.`);
			}

			this._DBs[this._currentDB].dropTable(name);
		} else {
			throw new DatabaseError(`Invalid target '${target}', target must be either 'database' or 'table'.`);
		}
	}

	/**
	 * Test if the target exist.
	 * @param {string} target Whether to test for a database or a table
	 * @param {string} name The name to test
	 * @returns {boolean} A boolean
	 */
	public exist(
		target: "database" | "table", 
		name: string
	): boolean {
		if (target === "database") {
			assertString(name, `Invalid type of database name '${typeof name}', it must be a string.`);

			return name in this._DBs;
		} else if (target === "table") {
			assertString(name, `Invalid type of table name '${typeof name}', it must be a string.`);

			return this._DBs[this._currentDB].exist(name);
		} else {
			throw new DatabaseError(`Invalid target '${target}', target must be either 'database' or 'table'.`);
		}
	}

	/**
	 * Specify which database to use
	 * @param {string} databaseName The database name
	 */
	public use(
		databaseName: string
	): void {
		assertString(databaseName, `Invalid type of database name '${typeof databaseName}', it must be a string.`)

		if (!this.exist("database", databaseName)) {
			throw new DatabaseUseError(`Unable to use database '${databaseName}', the database does not exist.`);
		}

		this._currentDB = databaseName;
	}

	/*public select(tableName: string, columnsName: string | string[] | { [key: string]: string }, conditions: string[] = []): DatabaseValueEntry[] {
		assertString(tableName, `Invalid type of table name '${typeof tableName}', it must be a string.`);
		
		const columns: { [key: string]: string } = {};

		if (typeof columnsName === "string") {
			columns[columnsName] = columnsName;
		} else if (typeof columnsName === "object") {
			if (Array.isArray(columnsName)) {
				columnsName.forEach(column => columns[column] = column);
			} else {
				for (let column in columnsName) {
					columns[column] = columnsName[column];
				}
			}
		} else {
			throw new TypeError(`Invalid type of columns name, it must be a string, an array of strings, or an object of strings.`);
		}

		if (!Array.isArray(conditions)) {
			throw new TypeError(`Invalid type of conditions, it must be an array of strings.`);
		}

		return this._DBs[this._currentDB].select(tableName, columns, conditions);
	}*/

	/**
	 * Select and query the database for entries
	 * @param {string | string[] | object} columnsName The columns to select
	 * @param {object} sources The sources
	 * @param {string[]} conditions The conditions to that need to evaluate to true
	 */
	public select(
		columnsName: string | string[] | { [key: string]: string }, 
		sources: { from: string, join: { inner?: { table: string, on: string }[] } }, 
		conditions: string[] = []
	): DatabaseValueEntry[] {
		let columns: { [key: string]: { [key: string]: string } } | "*" = {};

		assert(sources, "object", `Invalid source format.`);

		const tableName = sources.from;

		assertString(tableName, `Invalid table name '${tableName}'`);
		
		if (typeof columnsName === "string") {
			if (columnsName === "*") {
				columns = "*";
			} else {
				const { table, property } = parseProp(columnsName, tableName);

				columns[table] = { [property]: columnsName };
			}
		} else if (typeof columnsName === "object") {
			if (Array.isArray(columnsName)) {
				columnsName.forEach(column => {
					assertString(column ,`Columns must be strings`);

					const { table, property } = parseProp(column, tableName);

					if (!(table in (columns as { [key: string]: { [key: string]: string } }))) {
						columns[table] = {};
					}

					columns[table][property] = column;
				});
			} else {
				for (let column in columnsName) {
					assertString(column ,`Columns must be strings`);

					const { table, property } = parseProp(column, tableName);
					const value: string = columnsName[column];

					assertString(value, `Columns name must be strings`);

					if (!(table in columns)) {
						columns[table] = {};
					}

					columns[table][property] = value;
				}
			}
		} else {
			throw new TypeError(`Invalid type of columns name, it must be a string, an array of strings, or an object of strings.`);
		}

		const join = sources.join || {};

		if (!Array.isArray(conditions)) {
			throw new TypeError(`Invalid type of conditions, it must be an array of strings.`);
		}

		conditions.forEach(condition => assertString(condition, `Conditions must be strings`));

		if (!this.exist("database", this._currentDB)) {
			throw new DatabaseUseError(`No database is currently in use.`);
		}

		return this._DBs[this._currentDB].select(tableName, columns, join, conditions);
	}

	/**
	 * Inserts entries inside a table
	 * @param {string} tableName The table
	 * @param {DatabaseValueEntry} values The value(s) to insert
	 */
	public insert(
		tableName: string, 
		...values: DatabaseValueEntry[]
	): void {
		assertString(tableName, `Invalid type of table name '${typeof tableName}', it must be a string.`);

		if (!this.exist("database", this._currentDB)) {
			throw new DatabaseUseError(`No database is currently in use.`);
		}

		values.forEach(value => {
			assert(value, "object", `Invalid type of values, it must be an object.`);

			for (let key in value) {
				assert(value[key], [ "string", "number", "boolean" ], `Invalid type of value.`);
			}

			this._DBs[this._currentDB].insert(tableName, value);
		});

		this._syncFunc.forEach(func => func.call(undefined, this._currentDB, tableName));
	}

	/**
	 * Updates entries from a table
	 * @param {string} tableName The table name
	 * @param {DatabaseValueEntry} values The columns to update
	 * @param {string[]} conditions The conditions to meet to upgrade the entry
	 */
	public update(
		tableName: string, 
		values: DatabaseValueEntry, 
		conditions: string[] = []
	): void {
		assertString(tableName, `Invalid type of table name '${typeof tableName}', it must be a string.`);
		assert(values, "object", `Invalid type of values, it must be an object.`);

		assert(values, "object", `Invalid type of values, it must be an object.`);

		for (let key in values) {
			assert(values[key], [ "string", "number", "boolean" ], `Invalid type of value.`);
		}

		if (!Array.isArray(conditions)) {
			throw new TypeError(`Invalid type of conditions, it must be an array of strings.`);
		}

		conditions.forEach(condition => assertString(condition, `Conditions must be strings`));

		this._DBs[this._currentDB].update(tableName, values, conditions);

		this._syncFunc.forEach(func => func.call(undefined, this._currentDB, tableName));
	}

	/**
	 * Delete entries from a table
	 * @param {string} tableName The table name
	 * @param {string[]} conditions The conditions to meet to delete entries
	 */
	public delete(
		tableName: string, 
		conditions: string[] = []
	): void {
		assertString(tableName, `Invalid type of table name '${typeof tableName}', it must be a string.`);

		if (!Array.isArray(conditions)) {
			throw new TypeError(`Invalid type of conditions, it must be an array of strings.`);
		}

		conditions.forEach(condition => assertString(condition, `Conditions must be strings`));

		this._DBs[this._currentDB].delete(tableName, conditions);

		this._syncFunc.forEach(func => func.call(undefined, this._currentDB, tableName));
	}

	/**
	 * Backup a database
	 * @param {string} databaseName The name of the database to backup
	 * @returns {string} The database backup represented as a JSON object
	 */
	public backup(
		databaseName: string
	): string {
		assertString(databaseName, `Invalid type of database name '${typeof databaseName}', it must be a string.`)

		if (!this.exist("database", this._currentDB)) {
			throw new DatabaseError(`Unable to backup database '${databaseName}', the database does not exist.`);
		}

		return JSON.stringify({ database: this._DBs[databaseName] });
	}

	/**
	 * Restore a database from a JSON object
	 * @param {string} data The JSON object
	 * @param {string} databaseName The name of the databasee to restore to
	 */
	public restore(
		data: string, 
		databaseName: string
	): void {
		assertString(data, `Invalid backup format, it must be a string.`);
		assertString(databaseName, `Invalid type of database name '${typeof databaseName}', it must be a string.`)

		if (this.exist("database", databaseName)) {
			throw new DatabaseCreationError(`Unable to restore to database '${databaseName}', the database already exist.`);
		}
		
		const source = JSON.parse(data);

		this._DBs[databaseName] = new Database(databaseName);

		for (const table in source.database._tables) {
			this._DBs[databaseName].createTable(table, source.database._tables[table]._columns, source.database._tables[table]._identity || 0);

			for (const entry in source.database._tables[table]._data) {
				this._DBs[databaseName].insert(table, source.database._tables[table]._data[entry], { noIdentityError: true });
			}
		}
	}

	/**
	 * Add a function that will be trigered every time data inside tables are modified
	 * @param {DatabaseSyncFunction} func The function to add to the synchronize chain
	 */
	public sync(
		func: DatabaseSyncFunction
	): void {
		assert(func, "function", `The synchronize function must be a function.`);

		this._syncFunc.push(func);
	}
}