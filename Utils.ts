/// <reference lib="es2018.regexp" />

import { DatabaseAssertionError } from "./Errors.js";
import { TableDefinition } from "./TableDefinition.js";

const DateRegexp = "[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])";
const TimeRegexp = "([01][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9])";

/**
 * Asserts that 'value' is the correct Database type, else throw a DatabaseAssertionError
 * @param value The value to assert
 * @param source The type to assert
 */
export function assertType(value: any, source: TableDefinition["child"]): void | never {
	const { type } = source;

	switch (type) {
		case "bit":
			if (typeof value === "number" && (value === 0 || value === 1)) {
				return;
			}

			break;
		case "char":
			if (typeof value === "string" && value.length === 1) {
				return;
			}

			break;
		case "date":
			if (typeof value === "string" && new RegExp(`^${DateRegexp}$`).test(value)) {
				return;
			}

			break;
		case "datetime":
			if (typeof value === "string" && new RegExp(`^${DateRegexp}T${TimeRegexp}Z$`).test(value)) {
				return;
			}

			break;
		case "float":
			if (typeof value === "number") {
				return;
			}

			break;
		case "int":
			if (typeof value === "number" && Number.isInteger(value)) {
				return;
			}

			break;
		case "string":
			if (typeof value === "string") {
				return;
			}

			break;
		case "time":
			if (typeof value === "string" && new RegExp(`^${TimeRegexp}$`).test(value)) {
				return;
			}

			break;
		case "varchar":
			if (typeof value === "string" && value.length <= (source.maxLength as number)) {
				return;
			}

			break;
		case "boolean":
			if (typeof value === "boolean") {
				return;
			}

			break;
		default:
			throw new TypeError(`Invalid type '${type}'.`);
	}

	throw new DatabaseAssertionError(`Value '${value}' does not respect type '${type}'`);
}

/**
 * Assert that 'value' is of native types defined in 'type', else throw a TypeError
 * @param value The value to assert
 * @param type The type(s) to assert
 * @param errorMessage The message thrown using a TypeError
 */
export function assert(value: any, type: string | string[], errorMessage?: string): void | never {
	const types: string[] = ([] as string[]).concat(type);

	if (!types.includes(typeof value)) {
		throw new TypeError(errorMessage);
	}

	return;
}

/**
 * Assert that 'value' is a string, else throw a TypeError
 * @param value The value to assert
 * @param errorMessage The message thrown using a TypeError
 */
export function assertString(value: any, errorMessage?: string): void | never {
	assert(value, "string", errorMessage);

	return;
}

/**
 * Test if 'value' is nullable (null or undefined)
 * @param value The value to test
 * @returns A boolean
 */
export function isNullable(value: any): boolean {
	return value === undefined || value === null;
}

/**
 * Parse a string to extract the table name and the table column
 * @param key The string to parse
 * @param tableName The table name to use as default if none is matched
 * @returns .table: The table name
 * @returns .property: The column name
 */
export function parseProp(key: string, tableName?: string): { table: string, property: string } {
	const { table, property, prop } = (key.match(/(?:(?<table>\w+)\.(?<property>\w+)|(?<prop>\w+))/) as RegExpMatchArray).groups as { table: string, property: string, prop: string };

	return { table: table || tableName || "", property: property || prop };
}

/**
 * Assert a correct structure of the table definition
 * @param {TableDefinition} definition
 */
export function assertDefinition(definition: TableDefinition): void | never {
	assert(definition, "object", `Invalid table definition, it must be an object.`);

	for (let key in definition) {
		assert(definition[key], "object", `Invalid column ddefinition, it must be an object.`);

		for (let entry in definition[key]) {
			switch (entry) {
				case "type":
					switch (definition[key][entry]) {
						case "bit":
						case "float":
						case "int":
						case "date":
						case "time":
						case "datetime":
						case "varchar":
						case "string":
						case "boolean":
							break;
						default:
							throw new TypeError(`Invalid column type '${definition[key][entry]}'.`);
					}
					break;
				case "maxLength":
					if (typeof definition[key][entry] !== "number") {
						throw new TypeError(`Invalid typeof value for maxLength property.`)
					}

					break;
				case "default":
					assertType(definition[key][entry], definition[key]);

					break;
				case "identity":
					if (typeof definition[key][entry] !== "boolean") {
						throw new TypeError(`Invalid typeof value for identity property, it must be a boolean.`);
					}

					break;
				case "unique":
					if (typeof definition[key][entry] !== "boolean") {
						throw new TypeError(`Invalid typeof value for unique property, it must be a boolean.`);
					}

					break;
				case "nonNull":
					if (typeof definition[key][entry] !== "boolean") {
						throw new TypeError(`Invalid typeof value for nonNull property, it must be a boolean.`);
					}

					break;
				case "check":
					if (!Array.isArray(definition[key][entry])) {
						throw new TypeError(`Invalid typeof value for check property, it must be an array.`);
					}

					(definition[key][entry] as string[]).forEach(check => assertString(check, `Checks must be an array of strings.`));

					break;
				default:
					throw new SyntaxError(`Invalid definition structure`);
			}
		}
	}
}