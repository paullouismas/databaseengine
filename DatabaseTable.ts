import { DatabaseValueEntry } from "./DatabaseValueEntry.js";
import { TableDefinition } from "./TableDefinition.js";
import { assertType, isNullable } from "./Utils.js";
import { DatabaseAssertionError, TableError, TableCreationError } from "./Errors.js";
import { DatabaseConditions } from "./DatabaseConditions.js";
import { DatabaseConfig } from "./DatabaseConfig.js";

export class DatabaseTable {
	private _columns: TableDefinition = {};

	private _data: DatabaseValueEntry[] = [];

	private _name: string;

	private _identity: number;

	public constructor(
		tableName: string, 
		tableDefinition: TableDefinition, 
		identityCount: number
	) {
		this._name = tableName;
		this._identity = identityCount;

		for (let columnName in tableDefinition) {
			if (/[^\w]/.test(columnName)) {
				throw new TableCreationError(`Invalid character(s) in table columns name '${columnName}', it must only contain alphanumeric characters.`);
			}

			this._columns[columnName] = tableDefinition[columnName];

			if (tableDefinition[columnName].type === "varchar") {
				this._columns[columnName].maxLength = tableDefinition[columnName].maxLength;
			}
		}
	}

	public insert(
		entry: DatabaseValueEntry, 
		config: DatabaseConfig
	): void {
		const insertion: DatabaseValueEntry = {};

		for (let key in entry) {
			const value = entry[key];

			if (!(key in this._columns)) {
				throw new DatabaseAssertionError(`Column '${key}' does not exist inside table.`);
			}

			assertType(value, this._columns[key]);

			const { identity } = this._columns[key];

			if (!!identity  && !config.noIdentityError) {
				throw new DatabaseAssertionError(`Column '${key}' is an identity column and it's value cannot be assigned manually.`);
			}

			insertion[key] = value;
		}

		for (let key in this._columns) {
			if (!(key in insertion)) {
				if (!!this._columns[key].identity) {
					insertion[key] = this._identity += 1;

					continue;
				}

				insertion[key] = (isNullable(this._columns[key].default) ? null : this._columns[key].default) as string | number;
			}
		}

		for (let key in insertion) {
			const value = insertion[key];

			const { identity, unique, nonNull, check } = this._columns[key];

			if (!!unique) {
				if (this._data.reduce((accumulator, val) => {
					if (val[key] === value) {
						accumulator.push(val);
					}

					return accumulator;
				}, [] as DatabaseValueEntry[]).length > 0) {
					throw new DatabaseAssertionError(`Column '${key}' does not allow multiple identical values.`);
				}
			}

			if (!!nonNull && isNullable(value)) {
				throw new DatabaseAssertionError(`Column '${key}' does not allow null values.`);
			}

			if (!!check && !DatabaseConditions.evaluate(check, entry, this._name)) {
				throw new DatabaseAssertionError(`Entry does not pass checks.`);
			}
		}

		this._data.push(insertion);
	}

	public select(
		columns: { [key: string]: string } | "*", 
		join: object, 
		conditions: string[]
	): DatabaseValueEntry[] {
		// #TODO

		const returns: DatabaseValueEntry[] = [];

		for (let entry in this._data) {
			const rtn = {};
			const entr = JSON.parse(JSON.stringify(this._data[entry]));

			if (!DatabaseConditions.evaluate(conditions, entr, this._name)) {
				continue;
			}

			if (columns === "*") {
				returns.push(entr);

				continue;
			}

			for (let column in entr) {
				if (column in columns) {
					rtn[columns[column]] = entr[column];
				}
			}

			if (Object.keys(rtn).length > 0) {
				returns.push(rtn);
			}
		}

		return returns;
	}

	public delete(
		conditions: string[]
	): void {
		const indexes: number[] = this._data.reduce<number[]>((accumulator, value, index) => {
			if (DatabaseConditions.evaluate(conditions, value, this._name)) {
				accumulator.push(index);
			}

			return accumulator;
		}, []).sort((a, b) => b - a);

		indexes.forEach(index => this._data.splice(index, 1));
	}

	public update(
		values: DatabaseValueEntry, 
		conditions: string[]
	): void {
		for (let entry in this._data) {
			if (DatabaseConditions.evaluate(conditions, this._data[entry], this._name)) {
				for (let value in values) {
					if (!(value in this._columns)) {
						throw new TableError(`Invalid column '${value}'. Column does not exist.`);
					}

					assertType(values[value], this._columns[value]);

					this._data[entry][value] = values[value];
				}
			}
		}
	}
}