export class DatabaseError extends Error {
	public constructor(message?: string) {
		super(message);

		this.name = "DatabaseError";
	}
}
export class DatabaseCreationError extends DatabaseError {
	public constructor(message?: string) {
		super(message);

		this.name = "DatabaseCreationError";
	}
}
export class DatabaseUseError extends DatabaseError {
	public constructor(message?: string) {
		super(message);

		this.name = "DatabaseUseError";
	}
}
export class DatabaseAssertionError extends DatabaseError {
	public constructor(message?: string) {
		super(message);

		this.name = "DatabaseAssertionError";
	}
}
export class TableError extends Error {
	public constructor(message?: string) {
		super(message);

		this.name = "TableError";
	}
}
export class TableCreationError extends TableError {
	public constructor(message?: string) {
		super(message);

		this.name = "TableCreationError";
	}
}
export class TableInsertError extends TableError {
	public constructor(message?: string) {
		super(message);

		this.name = "TableInsertError";
	}
}