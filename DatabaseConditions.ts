import { DatabaseValueEntry } from "./DatabaseValueEntry.js";
import { DatabaseError } from "./Errors.js";
import { assertType } from "./Utils.js";
import { DatabaseTypes } from "./DatabaseTypes.js";

export class DatabaseConditions {
	private static _functions: {} = {
		YEAR(date: DatabaseTypes): number {
			assertType(date, { type: "date" });

			return Number.parseInt((date.match(/^([0-9]{4})-(?:[0-9]{2})-(?:[0-9]{2})$/) as string[])[1]);
		}, 
		MONTH(date: DatabaseTypes): number {
			assertType(date, { type: "date" });

			return Number.parseInt((date.match(/^(?:[0-9]{4})-([0-9]{2})-(?:[0-9]{2})$/) as string[])[1]);
		}, 
		DAY(date: DatabaseTypes): number {
			assertType(date, { type: "date" });

			return Number.parseInt((date.match(/^(?:[0-9]{4})-(?:[0-9]{2})-([0-9]{2})$/) as string[])[1]);
		}, 
		LEN(text: DatabaseTypes): number {
			assertType(text, { type: "string" });

			return text.length;
		}, 
		GETDATE(): string {
			const now = new Date();

			return `${now.getFullYear()}-${(now.getMonth() + 1).toString().padStart(2, "0")}-${now.getDate().toString().padStart(2, "0")}`;
		}
	};

	public static evaluate(conditions: string[], entry: DatabaseValueEntry, tableName: string): boolean {
		conditions = conditions.map(condition => {
			for (let column in entry) {
				const value = entry[column];

				condition = condition.replace(new RegExp(`\{\{${tableName}.${column}\}\}`, "g"), value as string);
			}

			condition = condition.replace(/(?<cmd>[A-Z]+)\((?<arg>[^)]*)\)/g, (...args: any[]): string => {
				const { cmd, arg } = args.reduce((accumulator, value) => typeof value === "object" ? value : accumulator, {});
				const command = ((cmd || "") as string).toUpperCase();

				if (!(command in this._functions)) {
					throw new DatabaseError(`Unknown function '${cmd}'.`);
				};

				return this._functions[command](arg);
			})

			return condition;
		});

		return conditions.every(condition => Function(`try { return ${condition}; } catch (error) { return false; }`)());
	}
}