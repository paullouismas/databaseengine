export type DatabaseValueEntry = {
	[key: string]: string | number;
};