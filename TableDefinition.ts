import { DatabaseTypes } from "./DatabaseTypes.js";

export type TableDefinition = {
	[key: string]: {
		type: DatabaseTypes;
		maxLength?: number;
		default?: number | string;
		identity?: boolean;
		unique?: boolean;
		nonNull?: boolean;
		check?: string[];
	}
};